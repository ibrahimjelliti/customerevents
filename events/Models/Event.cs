﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events.Models
{
    public class Event
    {
        public string Name { get; set; }
        public string City { get; set; }
        public int Price { get; set; }
    }
}
