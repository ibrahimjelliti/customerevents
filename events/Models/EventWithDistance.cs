﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace events.Models
{
    public class EventWithDistance : Event
    {
        public double Distance { get; set; }
        public EventWithDistance(string name,string city, double distance, int price = 0)
        {
            Name = name;
            City = city;
            Distance = distance;
            Price = price;
        }
    }
}
