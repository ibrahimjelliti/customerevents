﻿using events.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace events
{
    internal class Program
    {
        public static Dictionary<string, int> cacheCitiesDistances = new Dictionary<string, int>();
        static void Main(string[] args)
        {
            var events = new List<Event> {
                new Event{ Name = "Phantom of the Opera", City = "New York"},
                new Event{ Name = "Metallica", City = "Los Angeles"},
                new Event{ Name = "Metallica", City = "New York"},
                new Event{ Name = "Metallica", City = "Boston"},
                new Event{ Name = "LadyGaGa", City = "New York"},
                new Event{ Name = "LadyGaGa", City = "Boston"},
                new Event{ Name = "LadyGaGa", City = "Chicago"},
                new Event{ Name = "LadyGaGa", City = "San Francisco"},
                new Event{ Name = "LadyGaGa", City = "Washington"}
            };

            //var customer = new Customer { Name = "Mr. Fake", City = "New York" };
            var customer = new Customer { Name = "John Smith", City = "New York" };
            
            // Question 1: events of X customer
            Console.WriteLine("1) Events in the customer's location to the email\n");
            List<Event> eventsOfCustomer = EventsInCustomerCity(customer, events);
            foreach (var e in eventsOfCustomer)
            {
                AddToEmail(customer, e);
            }
            Console.WriteLine();

            // Question 2: closest 5 event of customer
            Console.WriteLine("2) Send events in the customer's location to the email with 5 closest near event\n");
            List<EventWithDistance> eventsOfCutomerWithDistance = CustomerlWithClosestEvents(events, customer, 5);
            EmailEventsToCustomer(customer, eventsOfCutomerWithDistance);


            // Question 3 and 4: optimization for question 2 with Dictionary- (accept fail)
            Console.WriteLine("3-4) Optimization: Send events in the customer's location to the email with 5 closest near event:\n");
            List<EventWithDistance> eventsOfCutomerWithDistanceCached = CustomerlWithClosestEventsCached(events, customer, 5);
            EmailEventsToCustomer(customer, eventsOfCutomerWithDistanceCached);


            // Question 5: with Price Sorting
            Console.WriteLine("5) Optimization: Send events in the customer's location to the email with 5 closest near event with price order:\n");
            List<EventWithDistance> eventsOfCustomerWithDistaneAndPrice = CustomerlWithClosestEventsAndPriceCached(events, customer, 5);
            EmailEventsToCustomer(customer, eventsOfCustomerWithDistaneAndPrice);

            Console.ReadKey();
        }

        private static void EmailEventsToCustomer(Customer customer, List<EventWithDistance> events)
        {
            foreach (var e in events)
            {
                AddToEmail(customer, e, e.Price);
            }
            Console.WriteLine();
        }

        private static List<EventWithDistance> CustomerlWithClosestEvents(List<Event> events, Customer customer, int limit)
        {
            return events
                .Select(ed => new EventWithDistance(
                    ed.Name,
                    ed.City,
                    GetDistance(customer.City, ed.City)))
                .OrderBy(e => e.Distance).Take(limit).ToList();

        }
        private static List<EventWithDistance> CustomerlWithClosestEventsCached(List<Event> events, Customer customer, int limit)
        {
            return events
                .Select(ed => new EventWithDistance(
                    ed.Name,
                    ed.City,
                    GetCachedDistance(customer.City, ed.City)))
                .OrderBy(e => e.Distance)
                .Take(limit)
                .ToList();
        }

        private static List<EventWithDistance> CustomerlWithClosestEventsAndPriceCached(List<Event> events, Customer customer, int limit)
        {
            return events
                .Select(ed => new EventWithDistance(
                    ed.Name,
                    ed.City,
                    GetCachedDistance(customer.City, ed.City), GetPrice(ed)))
                .OrderBy(e => e.Distance)
                .ThenBy(e => e.Price)
                .Take(limit)
                .ToList();
        }
        private static double GetCachedDistance(string city1, string city2)
        {
            int dist = 0;
            if (city1 == city2)
            {
                return dist;
            }
            var dictKey = $"{city1}-{city2}";
            bool keyexist = cacheCitiesDistances.TryGetValue(dictKey, out dist);
            if (!keyexist)
            {
                cacheCitiesDistances.TryGetValue($"{city2}-{city1}", out dist);
            }
            if (!keyexist)
            {
                try
                {
                    dist = GetDistance(city1, city2);
                    cacheCitiesDistances.Add(dictKey, dist);
                }
                catch (Exception)
                {
                    return dist;
                }
            }
            return dist;
        }
        private static List<Event> EventsInCustomerCity(Customer customer, List<Event> events)
            => events.Where(x => x.City.Equals(customer.City, StringComparison.OrdinalIgnoreCase)).ToList();

        // You do not need to know how these methods work
        static void AddToEmail(Customer c, Event e, int? price = null)
        {
            var distance = GetDistance(c.City, e.City);
            Console.Out.WriteLine($"{c.Name}: {e.Name} in {e.City}"
            + (distance > 0 ? $" ({distance} miles away)" : "")
            + (price.HasValue ? $" for ${price}" : ""));
        }

        static int GetPrice(Event e)
        {
            return (AlphebiticalDistance(e.City, "") + AlphebiticalDistance(e.Name, "")) / 10;
        }
        static int GetDistance(string fromCity, string toCity)
        {
            return AlphebiticalDistance(fromCity, toCity);
        }
        private static int AlphebiticalDistance(string s, string t)
        {
            var result = 0;
            var i = 0;
            for (i = 0; i < Math.Min(s.Length, t.Length); i++)
            {
                // Console.Out.WriteLine($"loop 1 i={i} {s.Length} {t.Length}");
                result += Math.Abs(s[i] - t[i]);
            }
            for (; i < Math.Max(s.Length, t.Length); i++)
            {
                // Console.Out.WriteLine($"loop 2 i={i} {s.Length} {t.Length}");
                result += s.Length > t.Length ? s[i] : t[i];
            }
            return result;
        }
    }
}
